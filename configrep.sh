#!/bin/bash

usage() {
  printf 'USAGE:\n'
  printf '  kubectl configrep [-n <namespace> | -A ] <pattern>\n'
  printf '\n'
  printf '  kubectl configrep <pattern>                  : list configmaps matching pattern in current namespace\n'
  printf '  kubectl configrep -n <namespace> <pattern>   : list configmaps matching pattern in provided namespace\n'
  printf '  kubectl configrep -A <pattern>               : list configmaps matching pattern in all namespaces\n'
  printf '\n'
}

current_namespace() {
  kubectl config view --minify --output 'jsonpath={..namespace}'
}

grep_configmap() {
  pattern="$1"

  [[ -z "$2" ]] && namespace="$(current_namespace)" || namespace="$2"

  # create an array holding the configmaps
  mapfile -t -d ' ' configmaps < <( kubectl get configmap -o jsonpath='{.items[*].metadata.name}' -n "$namespace" )

  printf ' Namespace: %s\n' "$namespace"
  # check each configmap for supplied pattern
  for i in "${configmaps[@]}"; do
    if kubectl get cm "$i" \
          -n "$namespace" \
          -o jsonpath='{.data}' |\
          grep -q "$pattern"; then
      echo "    $i"
    fi
  done
}



while getopts An:h opt; do
  case $opt in
    n)
      namespace="$OPTARG"
      shift 2
      grep_configmap  "$@" "$namespace"
      exit $?
      ;;
    A)
      shift
      # create an array holding the namespaces
      mapfile -t -d ' ' namespaces < <( kubectl get ns -o jsonpath='{.items[*].metadata.name}' )
      for i in "${namespaces[@]}"; do
        grep_configmap "$@" "$i"
      done
      exit $?
      ;;
    h)
      usage
      exit 0
      ;;
    ?)
      echo "$0: unknown option $*"
      usage
      exit 1
      ;;
  esac
done


# no options, grep in current namespace
if (( OPTIND == 1 )); then
  shift $((OPTIND-1))
  grep_configmap "$1"
fi
