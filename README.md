![GitLab stars](https://img.shields.io/badge/dynamic/json?color=green&label=gitlab%20stars&query=%24.star_count&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F21752082)
![Proudly written in Bash](https://img.shields.io/badge/written%20in-bash-ff69b4.svg)


# kubectl configrep

Kubectl configrep plugin - finds patterns in configmaps


# Demo

**`configrep`** helps you find configmaps containing a pattern:

![configrep demo GIF](docs/demo.gif)


# Installation

You have different options of installing this plugin.

### Kubectl Plugins (macOS and Linux)

You can install and use [Krew](https://github.com/kubernetes-sigs/krew/) kubectl
plugin manager to get `configrep`.

```sh
kubectl krew install configrep
```

After installing, the plugin will be available as `kubectl configrep`.

### Manual

Since `configrep` is written in Bash, you should be able to install
it to any POSIX environment that has Bash installed.

- Download the `configrep.sh` script.
- Either:
  - save it to somewhere in your `PATH` as `kubectl-configrep`,
  - or save it to a directory, then create symlinks to `configrep.sh` from
    somewhere in your `PATH`, like `/usr/local/bin/kubectl-configrep`
- Make `configrep.sh`/`kubectl-configrep` executable (`chmod +x ...`)
